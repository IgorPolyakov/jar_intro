class Report < ApplicationRecord
  validates :event_id, uniqueness: true
  validates :event_id, :browser, :domain, presence: true
end
