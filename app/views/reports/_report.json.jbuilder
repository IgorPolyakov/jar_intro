json.extract! report, :id, :event_id, :browser, :domain, :created_at, :updated_at
json.url report_url(report, format: :json)